package com.example.diceroller

import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlin.random.Random.Default.nextInt
import android.media.MediaPlayer




class MainActivity : AppCompatActivity() {
    lateinit var rollbutton: Button
    lateinit var resetbutton: Button
    lateinit var dice1: ImageButton
    lateinit var dice2: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val images = arrayOf(R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3, R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6)
        val toast = Toast.makeText(applicationContext, "Jackpot", Toast.LENGTH_SHORT)

        rollbutton = findViewById(R.id.rollbutton)
        resetbutton = findViewById(R.id.resetbutton)
        dice1 = findViewById(R.id.dice1)
        dice2 = findViewById(R.id.dice2)

        rollbutton.setOnClickListener {
            dice1.setImageResource(images.random())
            dice2.setImageResource(images.random())

            val diceroll: MediaPlayer = MediaPlayer.create(this, R.raw.diceroll)
            diceroll.start()

            if (dice1.getDrawable().getConstantState() == getResources().getDrawable(images[5]).getConstantState()
                && dice2.getDrawable().getConstantState() == getResources().getDrawable(images[5]).getConstantState()) {
                toast.show()
            }
        }

        resetbutton.setOnClickListener{
            dice1.setImageResource(R.drawable.empty_dice)
            dice2.setImageResource(R.drawable.empty_dice)
        }

        dice1.setOnClickListener {
            dice1.setImageResource(images[nextInt(0,6)])
            if (dice1.getDrawable().getConstantState() == getResources().getDrawable(images[5]).getConstantState()
                && dice2.getDrawable().getConstantState() == getResources().getDrawable(images[5]).getConstantState()) {
                toast.show()
            }
        }

        dice2.setOnClickListener{
            dice2.setImageResource(images[nextInt(0,6)])
            if (dice1.getDrawable().getConstantState() == getResources().getDrawable(images[5]).getConstantState()
                && dice2.getDrawable().getConstantState() == getResources().getDrawable(images[5]).getConstantState()) {
                toast.show()
            }
        }
    }
}